package com.ail.pageflow.service;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ail.core.BaseException;
import com.ail.core.CoreContext;
import com.ail.core.CoreProxy;
import com.ail.core.PreconditionException;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.quotation.ApplyMTAQuotationService.ApplyMTAQuotationCommand;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.pageflow.PageFlowContext;

@RunWith(PowerMockRunner.class)
@PrepareForTest({PageFlowContext.class, CoreContext.class})
public class ApplyMTAQuotationServiceTest {

    private ApplyMTAQuotationService sut;

    @Mock
    private ExecutePageActionArgument args;
    @Mock
    private Policy mtaQuotation;
    @Mock
    private CoreProxy coreProxy;
    @Mock
    private ApplyMTAQuotationCommand applyMTAQuotationCommand;
    @Mock
    private Policy masterPolicy;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        mockStatic(PageFlowContext.class);
        mockStatic(CoreContext.class);

        when(CoreContext.getCoreProxy()).thenReturn(coreProxy);

        doReturn(mtaQuotation).when(args).getModelArgRet();
        doReturn(applyMTAQuotationCommand).when(coreProxy).newCommand(eq(ApplyMTAQuotationCommand.class));
        doReturn(masterPolicy).when(applyMTAQuotationCommand).getPolicyRet();

        sut = new ApplyMTAQuotationService();
        sut.setArgs(args);
    }

    @Test(expected = PreconditionException.class)
    public void confirmNullPolicyCausesException() throws BaseException {
        doReturn(null).when(args).getModelArgRet();
        sut.invoke();
    }

    @Test
    public void confirmCreateMTAQuotationCommandIsInvoked() throws BaseException {
        sut.invoke();

        verify(applyMTAQuotationCommand).setQuotationArg(mtaQuotation);
        verify(applyMTAQuotationCommand).invoke();
    }
}
