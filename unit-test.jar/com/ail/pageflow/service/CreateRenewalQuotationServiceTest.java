package com.ail.pageflow.service;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ail.core.BaseException;
import com.ail.core.CoreContext;
import com.ail.core.CoreProxy;
import com.ail.core.PreconditionException;
import com.ail.core.context.PreferencesWrapper;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.quotation.CreateRenewalQuotationService.CreateRenewalQuotationCommand;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.pageflow.PageFlowContext;

@RunWith(PowerMockRunner.class)
@PrepareForTest({PageFlowContext.class, CoreContext.class, CreateRenewalQuotationService.class})
public class CreateRenewalQuotationServiceTest {

    private CreateRenewalQuotationService sut;

    @Mock
    private ExecutePageActionArgument args;
    @Mock
    private Policy policy;
    @Mock
    private Policy renewal;
    @Mock
    private CoreProxy coreProxy;
    @Mock
    private CreateRenewalQuotationCommand createRenewalQuotationCommand;
    @Mock
    private PreferencesWrapper preferencesWrapper;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        mockStatic(PageFlowContext.class);
        mockStatic(CoreContext.class);

        when(CoreContext.getCoreProxy()).thenReturn(coreProxy);
        when(CoreContext.getPreferencesWrapper()).thenReturn(preferencesWrapper);

        doReturn(policy).when(args).getModelArgRet();
        doReturn(createRenewalQuotationCommand).when(coreProxy).newCommand(eq(CreateRenewalQuotationCommand.class));
        doReturn(renewal).when(createRenewalQuotationCommand).getRenewalRet();

        sut = new CreateRenewalQuotationService();
        sut.setArgs(args);
    }

    @Test(expected = PreconditionException.class)
    public void confirmNullPolicyCausesException() throws BaseException {
        when(args.getModelArgRet()).thenReturn(null);
        sut.invoke();
    }

    @Test
    public void confirmCreateRenewalQuotationCommandIsInvoked() throws BaseException {
        sut.invoke();

        verify(createRenewalQuotationCommand).setPolicyArg(eq(policy));
        verify(createRenewalQuotationCommand).invoke();
    }
}
