package com.ail.core.document;

import static fr.opensagres.xdocreport.template.TemplateEngineKind.Velocity;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import java.io.InputStream;
import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ail.core.BaseException;
import com.ail.core.document.RenderDocumentService.RenderDocumentArgument;

import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;

@RunWith(PowerMockRunner.class)
@PrepareForTest({URL.class, RenderXDocReportPdfDocumentService.class, XDocReportRegistry.class})
public class RenderXDocReportPdfDocumentServiceTest {

    private static final String TEMPLATE_URL_ARG = "TEMPLATE_URL_ARG";

    private RenderXDocReportPdfDocumentService sut;

    @Mock
    private XDocReportRegistry xDocReportRegistry;
    @Mock
    private RenderDocumentArgument args;
    @Mock
    private IXDocReport iXDocReport;
    @Mock
    private InputStream inputStream;

    private URL url;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        mockStatic(XDocReportRegistry.class);
        mockStatic(URL.class);

        sut = new RenderXDocReportPdfDocumentService();
        url = PowerMockito.mock(URL.class);

        sut.setArgs(args);

        when(XDocReportRegistry.getRegistry()).thenReturn(xDocReportRegistry);

        doReturn(TEMPLATE_URL_ARG).when(args).getTemplateUrlArg();
    }

    @Test
    public void testTemplateCacheHit() throws BaseException {
        doReturn(iXDocReport).when(xDocReportRegistry).getReport(eq(TEMPLATE_URL_ARG));

        IXDocReport result = sut.fetchReport();

        assertThat(result, is(iXDocReport));
        verify(xDocReportRegistry, times(2)).getReport(eq(TEMPLATE_URL_ARG));
    }

    @Test
    public void testTemplateCacheMiss() throws Exception {
        doReturn(null).doReturn(iXDocReport).when(xDocReportRegistry).getReport(eq(TEMPLATE_URL_ARG));

        whenNew(URL.class).withArguments(eq(TEMPLATE_URL_ARG)).thenReturn(url);
        when(url.openStream()).thenReturn(inputStream);

        IXDocReport result = sut.fetchReport();

        assertThat(result, is(iXDocReport));
        verify(xDocReportRegistry, times(2)).getReport(eq(TEMPLATE_URL_ARG));
        verify(xDocReportRegistry).loadReport(eq(inputStream), eq(TEMPLATE_URL_ARG), eq(Velocity));
    }
}
