package com.ail.insurance.search;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ail.core.BaseException;
import com.ail.core.PreconditionException;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.policy.PolicyStatus;
import com.ail.insurance.search.FilterListToCurrentlyActiveService.FilterListToCurrentlyActiveArgument;

public class FilterListToCurrentlyActiveServiceTest {

    private FilterListToCurrentlyActiveService sut;

    @Mock
    private FilterListToCurrentlyActiveArgument args;

    private List<Policy> policiesList;

    @Captor
    private ArgumentCaptor<List<Policy>> policiesRetCaptor;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        sut = new FilterListToCurrentlyActiveService();

        sut.setArgs(args);

        policiesList = new ArrayList<>();

        doReturn(policiesList).when(args).getPoliciesArg();
    }

    @Test(expected = PreconditionException.class)
    public void verifyThatNullPoliciesListThrowsPreconditionException() throws BaseException {
        doReturn(null).when(args).getPoliciesArg();

        sut.invoke();
    }

    @Test
    public void checkActivePoliciesReturnedOne() throws BaseException {
        Policy policy1 = new Policy();
        Policy policy2 = new Policy();
        Policy policy5 = new Policy();
        Policy policy6 = new Policy();
        Policy policy7 = new Policy();

        policy1.setStatus(PolicyStatus.ON_RISK);
        policy1.setSystemId(1);
        policy1.setPolicyNumber("POL123");
        policy1.setMtaIndex(0L);
        policy1.setRenewalIndex(0L);

        policy2.setStatus(PolicyStatus.ON_RISK);
        policy2.setSystemId(2);
        policy2.setPolicyNumber("POL123");
        policy2.setMtaIndex(1L);
        policy2.setRenewalIndex(0L);

        policy5.setStatus(PolicyStatus.APPLICATION);
        policy5.setSystemId(5);

        policy6.setStatus(PolicyStatus.ON_RISK);
        policy6.setPolicyNumber("POL456");
        policy6.setSystemId(6);

        policy7.setStatus(PolicyStatus.DELETED);
        policy7.setSystemId(7);
        policy7.setPolicyNumber("POL123");
        policy7.setMtaIndex(2L);
        policy7.setRenewalIndex(2L);

        List<Policy> policies = Arrays.asList(policy2, policy6, policy7, policy1, policy5);

        doReturn(policies).when(args).getPoliciesArg();

        sut.invoke();

        verify(args).setFilteredPoliciesRet(policiesRetCaptor.capture());

        Collection<Policy> filtered = policiesRetCaptor.getValue();

        assertEquals(3, filtered.size());

        Iterator<Policy> polIt = filtered.iterator();
        assertEquals(2L, polIt.next().getSystemId());
        assertEquals(6L, polIt.next().getSystemId());
        assertEquals(5L, polIt.next().getSystemId());
    }

    @Test
    public void checkActivePoliciesReturnedTwo() throws BaseException {
        Policy policy1 = new Policy();
        Policy policy2 = new Policy();
        Policy policy3 = new Policy();
        Policy policy4 = new Policy();
        Policy policy5 = new Policy();
        Policy policy7 = new Policy();

        policy1.setStatus(PolicyStatus.ON_RISK);
        policy1.setSystemId(1);
        policy1.setPolicyNumber("POL123");
        policy1.setMtaIndex(0L);
        policy1.setRenewalIndex(0L);

        policy2.setStatus(PolicyStatus.ON_RISK);
        policy2.setSystemId(2);
        policy2.setPolicyNumber("POL123");
        policy2.setMtaIndex(1L);
        policy2.setRenewalIndex(0L);

        policy3.setStatus(PolicyStatus.ON_RISK);
        policy3.setSystemId(3);
        policy3.setPolicyNumber("POL123");
        policy3.setMtaIndex(0L);
        policy3.setRenewalIndex(1L);

        policy4.setStatus(PolicyStatus.ON_RISK);
        policy4.setSystemId(4);
        policy4.setPolicyNumber("POL123");
        policy4.setMtaIndex(1L);
        policy4.setRenewalIndex(1L);

        policy5.setStatus(PolicyStatus.APPLICATION);
        policy5.setSystemId(5);

        policy7.setStatus(PolicyStatus.DELETED);
        policy7.setSystemId(7);
        policy7.setPolicyNumber("POL123");
        policy7.setMtaIndex(2L);
        policy7.setRenewalIndex(2L);

        List<Policy> policies = Arrays.asList(policy3, policy2, policy4, policy7, policy1, policy5);

        doReturn(policies).when(args).getPoliciesArg();

        sut.invoke();

        verify(args).setFilteredPoliciesRet(policiesRetCaptor.capture());

        Collection<Policy> filtered = policiesRetCaptor.getValue();

        assertEquals(2, filtered.size());

        Iterator<Policy> polIt = filtered.iterator();
        assertEquals(4L, polIt.next().getSystemId());
        assertEquals(5L, polIt.next().getSystemId());
    }
}
