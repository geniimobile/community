#!/bin/bash

function help {
	echo "post"    
}

function post {
	PAYLOAD='{"text":"'$ARG_message'"}'
	curl -X POST --data-urlencode payload="$PAYLOAD" https://hooks.slack.com/services/$SLACK_NOTIFICATION_WEBHOOK
}