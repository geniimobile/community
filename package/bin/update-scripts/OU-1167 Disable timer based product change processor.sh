#!/bin/bash

[ -z "$OU_HOME" ] && echo "OU_HOME is not set" && exit

STANDALONE_CONF="$OU_HOME/liferay-portal-6.2-ce-ga6/jboss-7.1.1/bin/standalone.conf"

if [ -f "$STANDALONE_CONF" ]; then
	if [ -z "`grep com.ail.core.command.product_change_event_processor.disabled $STANDALONE_CONF`" ]; then
		echo 'JAVA_OPTS="$JAVA_OPTS -Dcom.ail.core.command.product_change_event_processor.disabled=true"' >> $STANDALONE_CONF
	fi
fi
