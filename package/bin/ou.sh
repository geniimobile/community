#!/bin/bash

#| Name: ou
#| 
#| Usage: ou <function-group> <function> [arguments]
#|
#| ou is a wrapper script which provides access to a set of ou service and support scripts. 

. ${0%/*}/ou/ou-common.sh
[ -f ~/.ou ] && . ~/.ou

function help {
	sed -n 's:^#|::p' $0
}

function invoke-function {
	FUNCTION_SCRIPT="${0%/*}/ou/ou-${FUNCTION_GROUP}.sh"
	[ ! -f "$FUNCTION_SCRIPT" ] && usage "function-group: \"$FUNCTION_GROUP\" not found."
	
	. $FUNCTION_SCRIPT
	
	[ "$(type -t $FUNCTION)" != "function" ] && usage "function: \"$FUNCTION\" not found."
	
	debug-on

	eval $FUNCTION	
	
	debug-off
}

FUNCTION_GROUP=$1; shift
FUNCTION=$1; shift
	
[ -z "$FUNCTION_GROUP" ] && usage "function-group not specified."	
[ -z "$FUNCTION" ] && usage "function not specified."

for i in "$@"; do
	STRIPPED=${i#-}
	[[ "$STRIPPED" == *"="* ]] && eval ARG_${STRIPPED%=*}=\"${STRIPPED#*=}\"
	[[ "$STRIPPED" != *"="* ]] && eval ARG_$STRIPPED=yes
done

invoke-function