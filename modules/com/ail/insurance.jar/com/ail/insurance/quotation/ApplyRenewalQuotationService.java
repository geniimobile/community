/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

package com.ail.insurance.quotation;

import static com.ail.core.CoreContext.getCoreProxy;
import static com.ail.insurance.policy.PolicyLinkType.RENEWAL_QUOTATION_FROM;
import static com.ail.insurance.policy.PolicyStatus.APPLIED;
import static com.ail.insurance.policy.PolicyStatus.ON_RISK;
import static com.ail.insurance.policy.PolicyStatus.SUBMITTED;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.ail.annotation.ServiceArgument;
import com.ail.annotation.ServiceCommand;
import com.ail.annotation.ServiceImplementation;
import com.ail.core.BaseException;
import com.ail.core.CloneError;
import com.ail.core.PreconditionError;
import com.ail.core.PreconditionException;
import com.ail.core.Service;
import com.ail.core.command.Argument;
import com.ail.core.command.Command;
import com.ail.core.document.Document;
import com.ail.financial.PaymentRecord;
import com.ail.insurance.policy.AssessmentSheet;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.policy.PolicyLink;
import com.ail.insurance.policy.PolicyLinkType;
import com.ail.insurance.policy.Section;
import com.ail.insurance.quotation.ApplyRenewalQuotationService.ApplyRenewalQuotationArgument;
import com.ail.insurance.quotation.FinaliseRenewalService.FinaliseRenewalCommand;

/**
 * This service will:<ol>
 * <li>Accept a quotation (instance of {@link com.ail.insurance.policy.Policy Policy}) to be applied, return the modified target policy.</li>
 * <li>Only accept a quotation policy if its status is {@link PolicyStatys#SUBMITTED SUBMITTED}.</li>
 * <li>Only accept a quotation policy if it has exactly one policy link of type {@link PolicyLinkType#RENEWAL_QUOTATION_FROM RENEWAL_QUOTATION_FROM}</li>
 * <li>Only accept a quotation if its {@link PolicyLinkType#RENEWAL_QUOTATION_FROM RENEWAL_QUOTATION_FROM} policy link points to a policy with a status of {@link PolicyStatys#ON_RISK ON_RISK}.</li>
 * <li>The following updates are made:<ul>
 * <li>The quotation's inception and expiry dates are copied to the policy.</li>
 * <li>The quotation's assessment sheets are copied to the policy.</li>
 * <li>The quotation's quotation document (if there is one) is copied to the policy.</li>
 * <li>The quotation's payment schedule is copied to the policy.</li>
 * <li><b>The product specific {@link FinaliseRenewalService}</code> is invoked.</b></li>
 * <li>The policy's renewal counter is incremented.</li>
 * <li>The quotation's status is set to {@link PolicyStatys#APPLIED APPLIED}</li>
 * </ul></ol>
 */
@ServiceImplementation
public class ApplyRenewalQuotationService extends Service<ApplyRenewalQuotationArgument> {
    private static final long serialVersionUID = 3819563603833694389L;

    @ServiceArgument
    public interface ApplyRenewalQuotationArgument extends Argument {
        void setQuotationArg(Policy quotationArg);

        Policy getQuotationArg();

        void setPolicyRet(Policy policyRet);

        Policy getPolicyRet();
    }

    @ServiceCommand(defaultServiceClass = ApplyRenewalQuotationService.class)
    public interface ApplyRenewalQuotationCommand extends Command, ApplyRenewalQuotationArgument {
    }

    @Override
    public void invoke() throws BaseException {
        if (args.getQuotationArg() == null) {
            throw new PreconditionException("args.getQuotationArg() == null");
        }

        if (args.getQuotationArg().getStatus() != SUBMITTED) {
            throw new PreconditionException("args.getQuotationArg().getStatus() != SUBMITTED");
        }

        List<PolicyLink> policyLinksForRenewingQuotationFrom = fetchPolicyLinksForRenewingQuotationFrom();

        if (policyLinksForRenewingQuotationFrom.size() != 1) {
            throw new PreconditionException("policyLinksForRenewingQuotationFrom.size() != 1");
        }

        Policy renewingPolicy = fetchRenewingPolicy(policyLinksForRenewingQuotationFrom.get(0).getTargetPolicyId());

        if (renewingPolicy == null) {
            throw new PreconditionException("renewingPolicy == null");
        }

        if (renewingPolicy.getStatus() != ON_RISK) {
            throw new PreconditionException("renewingPolicy.getStatus() != ON_RISK");
        }

        Policy quotationPolicy = args.getQuotationArg();

        copyInceptionAndExpiryDates(quotationPolicy, renewingPolicy);

        copyQuotationDocument(quotationPolicy, renewingPolicy);

        copyAssessmentSheets(quotationPolicy, renewingPolicy);

        copyPaymentSchedule(quotationPolicy, renewingPolicy);

        incrementRenewalIndex(renewingPolicy);

        copyPaymentHistory(quotationPolicy, renewingPolicy);

        finaliseRenewal(quotationPolicy, renewingPolicy);

        quotationPolicy.setStatus(APPLIED);

        args.setPolicyRet(renewingPolicy);
    }

    private void copyPaymentHistory(Policy quotationPolicy, Policy parentPolicy) {
        quotationPolicy.getPaymentHistory().forEach(ph -> {
            try {
                parentPolicy.getPaymentHistory().add((PaymentRecord)ph.clone());
            } catch (CloneNotSupportedException e) {
                throw new PreconditionError(String.format("Failed to clone payment history record: %d from quoation: %d into parentPolicy: %d", ph.getSystemId(), quotationPolicy.getSystemId(), parentPolicy.getSystemId()));
            }
        });
    }

    private void finaliseRenewal(Policy quotationPolicy, Policy renewingPolicy) throws BaseException {
            FinaliseRenewalCommand frc = getCoreProxy().newCommand("FinaliseRenewal", FinaliseRenewalCommand.class);
            frc.setRenewingPolicyArg(renewingPolicy);
            frc.setRenewalQuotationArg(quotationPolicy);
            frc.invoke();
    }

    private void copyPaymentSchedule(Policy quotationPolicy, Policy renewingPolicy) {
        renewingPolicy.setPaymentDetails(quotationPolicy.getPaymentDetails());
    }

    private void copyAssessmentSheets(Policy quotationPolicy, Policy renewingPolicy) {
        renewingPolicy.setAssessmentSheetList(cloneAssessmentSheets(quotationPolicy.getAssessmentSheetList()));

        quotationPolicy.getSection().
                        stream().
                        forEach(sourceSecion -> {
                            Section targetSection;
                            if ((targetSection = findMatchingSection(sourceSecion, renewingPolicy)) != null) {
                                targetSection.setAssessmentSheetList(cloneAssessmentSheets(sourceSecion.getAssessmentSheetList()));
                            }
                        });
    }

    Map<String, AssessmentSheet> cloneAssessmentSheets(Map<String, AssessmentSheet> source) throws CloneError {
        Map<String, AssessmentSheet> target = new HashMap<>();

        try {
            for(String key: source.keySet()) {
                AssessmentSheet clone = (AssessmentSheet)source.get(key).clone();
                clone.markAsNotPersisted();
                target.put(key, clone);
            }
        } catch (CloneNotSupportedException e) {
            throw new CloneError("Failed to clone assessmentSheet: "+source, e);
        }

        return target;
    }

    private Section findMatchingSection(Section target, Policy policy) {
        Section candidate = policy.getSectionById(target.getId());

        if (candidate != null && candidate.getSectionTypeId().equals(target.getSectionTypeId())) {
            return candidate;
        }
        else {
            return null;
        }
    }

    private void incrementRenewalIndex(Policy renewingPolicy) {
        Long indx = renewingPolicy.getRenewalIndex();
        renewingPolicy.setRenewalIndex((indx != null) ? indx + 1 : 1);
    }

    private void copyQuotationDocument(Policy quotationPolicy, Policy renewingPolicy) {
        Document quotationDocument;

        if ((quotationDocument = quotationPolicy.retrieveQuotationDocument()) != null) {
            renewingPolicy.attachQuotationDocument(quotationDocument.getDocumentContent().getContent());
        }
    }

    private void copyInceptionAndExpiryDates(Policy quotationPolicy, Policy renewingPolicy) {
        renewingPolicy.setInceptionDate(quotationPolicy.getInceptionDate());
        renewingPolicy.setInforceDate(quotationPolicy.getInceptionDate());
        renewingPolicy.setExpiryDate(quotationPolicy.getExpiryDate());
    }

    private Policy fetchRenewingPolicy(long systemId) {
        return (Policy)getCoreProxy().queryUnique("get.policy.by.systemId", systemId);
    }

    private List<PolicyLink> fetchPolicyLinksForRenewingQuotationFrom() {
        return args.getQuotationArg().
                    getPolicyLink().
                    stream().
                    filter(pl -> pl.getLinkType() == RENEWAL_QUOTATION_FROM).
                    collect(Collectors.toList());
    }
}
