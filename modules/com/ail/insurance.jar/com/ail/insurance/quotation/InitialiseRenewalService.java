/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
package com.ail.insurance.quotation;

import com.ail.annotation.ServiceArgument;
import com.ail.annotation.ServiceCommand;
import com.ail.annotation.ServiceInterface;
import com.ail.core.command.Argument;
import com.ail.core.command.Command;
import com.ail.insurance.policy.Policy;

@ServiceInterface
public interface InitialiseRenewalService {

    @ServiceArgument
    public interface InitialiseRenewalArgument extends Argument {
        void setRenewingPolicyArg(Policy renewingPolicyArg);

        Policy getRenewingPolicyArg();

        void setRenewalQuotationArg(Policy renewalQuotationArg);

        Policy getRenewalQuotationArg();
    }

    @ServiceCommand
    public interface InitialiseRenewalCommand extends Command, InitialiseRenewalArgument {
    }
}
