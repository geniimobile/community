/* Copyright Applied Industrial Logic Limited 2006. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.pageflow;

import static com.ail.core.CoreContext.getCoreProxy;
import static com.ail.insurance.policy.PolicyLinkType.MTA_QUOTATION_FOR;
import static com.ail.insurance.policy.PolicyLinkType.RENEWAL_QUOTATION_FOR;
import static com.ail.insurance.policy.PolicyStatus.APPLICATION;
import static com.ail.insurance.policy.PolicyStatus.QUOTATION;
import static com.ail.insurance.policy.PolicyStatus.SUBMITTED;
import static com.ail.pageflow.PageFlowContext.getOperationParameters;
import static com.ail.pageflow.PageFlowContext.getRequestedOperation;
import static com.ail.pageflow.PageFlowContext.getRequestedOperationId;
import static java.lang.Integer.parseInt;
import static java.util.Arrays.asList;
import static java.util.regex.Pattern.compile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.ail.core.BaseException;
import com.ail.core.Type;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.policy.PolicyLink;
import com.ail.insurance.policy.PolicyLinkType;
import com.ail.insurance.policy.PolicyStatus;

/**
 * Display a list of the {@link com.ail.insurance.policy.PolicyLink PolicyLinks}
 * associated with the current policy.
 */
public class PolicyLinks extends PageContainer {

    private static final String I18N_POLICY_LINK_BUTTON_APPLY_RENEWAL = "i18n_policy_link_button_apply_renewal";
    private static final String I18N_POLICY_LINK_BUTTON_APPLY_MTA = "i18n_policy_link_button_apply_mta";
    private static final String I18N_POLICY_LINK_BUTTON_CONTINUE_RENEWAL = "i18n_policy_link_button_continue_renewal";
    private static final String I18N_POLICY_LINK_BUTTON_CONTINUE_MTA = "i18n_policy_link_button_continue_mta";
    private static final String I18N_POLICY_LINK_BUTTON_NTU = "i18n_policy_link_button_ntu";
    private static final String I18N_POLICY_LINK_BUTTON_OPEN = "i18n_policy_link_button_open";

    private static Map<MatrixKey,List<String>> buttonMatrix;
    private static String DEFAULT_FILTER = ".*";
    private static Pattern DEFAULT_PATTERN = compile(DEFAULT_FILTER);

    private String linkTypeFilter = DEFAULT_FILTER;
    private String policyStatusFilter = DEFAULT_FILTER;
    private transient Pattern linkTypePattern = DEFAULT_PATTERN;
    private transient Pattern policyStatusPattern = DEFAULT_PATTERN;

    private Map<String, CommandButtonAction> buttons;

    public PolicyLinks() {
        super();
        initialise();
    }

    private synchronized void initialise() {
        if (buttonMatrix == null) {
            buttonMatrix = new HashMap<>();

            buttonMatrix.put(new MatrixKey(RENEWAL_QUOTATION_FOR, SUBMITTED),   asList(I18N_POLICY_LINK_BUTTON_APPLY_RENEWAL, I18N_POLICY_LINK_BUTTON_NTU, I18N_POLICY_LINK_BUTTON_OPEN));
            buttonMatrix.put(new MatrixKey(RENEWAL_QUOTATION_FOR, APPLICATION), asList(I18N_POLICY_LINK_BUTTON_CONTINUE_RENEWAL, I18N_POLICY_LINK_BUTTON_NTU, I18N_POLICY_LINK_BUTTON_OPEN));
            buttonMatrix.put(new MatrixKey(RENEWAL_QUOTATION_FOR, QUOTATION),   asList(I18N_POLICY_LINK_BUTTON_CONTINUE_RENEWAL, I18N_POLICY_LINK_BUTTON_NTU, I18N_POLICY_LINK_BUTTON_OPEN));

            buttonMatrix.put(new MatrixKey(MTA_QUOTATION_FOR, SUBMITTED),   asList(I18N_POLICY_LINK_BUTTON_APPLY_MTA, I18N_POLICY_LINK_BUTTON_NTU, I18N_POLICY_LINK_BUTTON_OPEN));
            buttonMatrix.put(new MatrixKey(MTA_QUOTATION_FOR, APPLICATION), asList(I18N_POLICY_LINK_BUTTON_CONTINUE_MTA, I18N_POLICY_LINK_BUTTON_NTU, I18N_POLICY_LINK_BUTTON_OPEN));
            buttonMatrix.put(new MatrixKey(MTA_QUOTATION_FOR, QUOTATION),   asList(I18N_POLICY_LINK_BUTTON_CONTINUE_MTA, I18N_POLICY_LINK_BUTTON_NTU, I18N_POLICY_LINK_BUTTON_OPEN));
        }

        buttons = new HashMap<>();

        buttons.put(I18N_POLICY_LINK_BUTTON_APPLY_RENEWAL, null);
        buttons.put(I18N_POLICY_LINK_BUTTON_APPLY_MTA, null);
        buttons.put(I18N_POLICY_LINK_BUTTON_CONTINUE_RENEWAL, null);
        buttons.put(I18N_POLICY_LINK_BUTTON_CONTINUE_MTA, null);
        buttons.put(I18N_POLICY_LINK_BUTTON_NTU, null);
        buttons.put(I18N_POLICY_LINK_BUTTON_OPEN, null);
    }

    public String getLinkTypeFilter() {
        return linkTypeFilter;
    }

    public void setLinkTypeFilter(String linkTypeFilter) {
        this.linkTypeFilter = linkTypeFilter;
        this.linkTypePattern = null;
    }

    public String getPolicyStatusFilter() {
        return policyStatusFilter;
    }

    public void setPolicyStatusFilter(String policyStatusFilter) {
        this.policyStatusFilter = policyStatusFilter;
        this.policyStatusPattern = null;
    }

    private Pattern getLinkTypePattern() {
        return linkTypePattern != null ? linkTypePattern : (linkTypePattern = compile(linkTypeFilter));
    }

    private Pattern getPolicyStatusPattern() {
        return policyStatusPattern != null ? policyStatusPattern : (policyStatusPattern = compile(policyStatusFilter));
    }

    public List<PolicyLink> fetchPolicyLinks(Policy policy) {
        return policy.
               getPolicyLink().
               stream().
               filter(pl -> isPolicyLinkValid(pl)).
               collect(Collectors.toList());
    }

    private void setButtonAction(CommandButtonAction action, String buttonId) {
        if (action.getLabel() == null) {
            action.setLabel(buttonId);
        }
        action.setId(buttonId);
        buttons.put(buttonId, action);
    }

    public void setApplyRenewalButtonAction(CommandButtonAction action) {
        setButtonAction(action, I18N_POLICY_LINK_BUTTON_APPLY_RENEWAL);
    }

    public CommandButtonAction getApplyRenewalButtonAction() {
        return buttons.get(I18N_POLICY_LINK_BUTTON_APPLY_RENEWAL);
    }

    public void setApplyMtaButtonAction(CommandButtonAction action) {
        setButtonAction(action, I18N_POLICY_LINK_BUTTON_APPLY_MTA);
    }

    public CommandButtonAction getApplyMtaButtonAction() {
        return buttons.get(I18N_POLICY_LINK_BUTTON_APPLY_MTA);
    }

    public void setContinueRenewalButtonAction(CommandButtonAction action) {
        setButtonAction(action, I18N_POLICY_LINK_BUTTON_CONTINUE_RENEWAL);
    }

    public CommandButtonAction getContinueRenewalButtonAction() {
        return buttons.get(I18N_POLICY_LINK_BUTTON_CONTINUE_RENEWAL);
    }

    public void setContinueMtaButtonAction(CommandButtonAction action) {
        setButtonAction(action, I18N_POLICY_LINK_BUTTON_CONTINUE_MTA);
    }

    public CommandButtonAction getContinueMtaButtonAction() {
        return buttons.get(I18N_POLICY_LINK_BUTTON_CONTINUE_MTA);
    }

    public void setNtuButtonAction(CommandButtonAction action) {
        setButtonAction(action, I18N_POLICY_LINK_BUTTON_NTU);
    }

    public CommandButtonAction getNtuButtonAction() {
        return buttons.get(I18N_POLICY_LINK_BUTTON_NTU);
    }

    public void setOpenButtonAction(CommandButtonAction action) {
        setButtonAction(action, I18N_POLICY_LINK_BUTTON_OPEN);
    }

    public CommandButtonAction getOpenButtonAction() {
        return buttons.get(I18N_POLICY_LINK_BUTTON_OPEN);
    }

    private boolean isPolicyLinkValid(PolicyLink policyLink) {
        String policylinkType = "", targetPolicyStatus = "";

        policylinkType = policyLink.getLinkType().longName();

        if (!getLinkTypePattern().matcher(policylinkType).matches()) {
            return false;
        }

        Policy targetPolicy = fetchLinkedPolicy(policyLink);

        if (targetPolicy != null) {
            targetPolicyStatus = targetPolicy.getStatus().longName();
        }

        return getPolicyStatusPattern().matcher(targetPolicyStatus).matches();
    }

    public Policy fetchLinkedPolicy(PolicyLink policyLink) {
        return (Policy) getCoreProxy().queryUnique("get.policy.by.systemId", policyLink.getTargetPolicyId());
    }

    /**
     * Fetch a list of the buttons that are:
     * a) valid for the combination of policy link type & target policy status;
     * b) has a commandButton associated with the policyLinks page element to handle it.
     * @param linkType
     * @param status
     * @return List of command IDs.
     */
    public List<String> fetchButtons(PolicyLinkType linkType, PolicyStatus status) {
        List<String> ret = buttonMatrix.get(new MatrixKey(linkType, status));

        if (ret != null) {
            return ret.stream().filter(id -> buttons.get(id) != null).collect(Collectors.toList());
        }
        else {
            return new ArrayList<>();
        }
    }

    @Override
    public Type applyRequestValues(Type model) {
        return model;
    }

    @Override
    public boolean processValidations(Type model) {
        return false;
    }

    @Override
    public Type processActions(Type model) throws BaseException {
        if (!conditionIsMet(model) && getId().equals(getRequestedOperationId())) {
            return model;
        }

        CommandButtonAction button = buttons.get(getRequestedOperation());

        if (button != null) {
            int row = parseInt(getOperationParameters().getProperty("row"));

            PolicyLink policyLink = fetchPolicyLinks(PageFlowContext.getPolicy()).get(row);

            Policy policy = fetchLinkedPolicy(policyLink);

            Policy current = PageFlowContext.getPolicy();

            button.processActions(policy);

            PageFlowContext.setPolicy(current);
        }

        return PageFlowContext.getPolicy();
    }

    @Override
    public Type renderResponse(Type model) throws IllegalStateException, IOException {
        return executeTemplateCommand(model);
    }

    public class MatrixKey {
        private PolicyLinkType linkType;
        private PolicyStatus status;

        public MatrixKey(PolicyLinkType linkType, PolicyStatus status) {
            this.linkType = linkType;
            this.status = status;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((linkType == null) ? 0 : linkType.hashCode());
            result = prime * result + ((status == null) ? 0 : status.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            MatrixKey other = (MatrixKey) obj;
            if (linkType != other.linkType)
                return false;
            if (status != other.status)
                return false;
            return true;
        }
    }
}
