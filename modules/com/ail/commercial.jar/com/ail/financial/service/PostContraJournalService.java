/* Copyright Applied Industrial Logic Limited 2017. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.financial.service;

import static com.ail.core.CoreContext.getCoreProxy;
import static com.ail.financial.ledger.TransactionType.CREDIT;
import static com.ail.financial.ledger.TransactionType.DEBIT;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.ail.annotation.ServiceArgument;
import com.ail.annotation.ServiceCommand;
import com.ail.annotation.ServiceImplementation;
import com.ail.core.BaseException;
import com.ail.core.PreconditionException;
import com.ail.core.Service;
import com.ail.core.command.Argument;
import com.ail.core.command.Command;
import com.ail.financial.ledger.Journal;
import com.ail.financial.ledger.Journal.JournalBuilder;
import com.ail.financial.ledger.JournalLine;
import com.ail.financial.ledger.JournalLine.JournalLineBuilder;
import com.ail.financial.ledger.JournalType;
import com.ail.financial.service.PostContraJournalService.PostContraJournalArgument;
import com.ail.financial.service.PostJournalService.PostJournalCommand;

@ServiceImplementation
public class PostContraJournalService extends Service<PostContraJournalArgument> {

    @Override
    public void invoke() throws BaseException {
        if (args.getJournalArg() == null) {
            throw new PreconditionException("args.getJournalArg() == null");
        }

        if (args.getJournalArg().getHasBeenContrad() == true) {
            throw new PreconditionException("args.getJournalArg().getHasBeenContrad() == true");
        }

        Set<JournalLine> contraLines = new HashSet<>();

        for(JournalLine sourceLine: args.getJournalArg().getJournalLine()) {
            contraLines.add(
                    new JournalLineBuilder().
                            transactionType(sourceLine.getTransactionType() == CREDIT ? DEBIT : CREDIT).
                            using(sourceLine.getPaymentMethod()).
                            by(sourceLine.getAmount()).
                            ledger(sourceLine.getLedger()).
                            ofType(sourceLine.getType()).
                            build()
            );
        }

        Journal contraJournal = new JournalBuilder().
                                    contraOf(args.getJournalArg()).
                                    forOrigin(args.getJournalArg().getOrigin()).
                                    subject(args.getJournalArg().getSubject()).
                                    ofType(args.getJournalTypeArg() == null ? args.getJournalArg().getType() : args.getJournalTypeArg()).
                                    withReason(args.getReasonArg()).
                                    withTransactionDate(new Date()).
                                    withWrittenDate(args.getWrittenDateArg()).
                                    with(contraLines).
                                    build();

        PostJournalCommand pjc = getCoreProxy().newCommand(PostJournalCommand.class);
        pjc.setJournalArgRet(contraJournal);
        pjc.invoke();

        args.getJournalArg().setHasBeenContrad(true);

        args.setContraRet(pjc.getJournalArgRet());
    }

    @ServiceCommand(defaultServiceClass = PostContraJournalService.class)
    public interface PostContraJournalCommand extends Command, PostContraJournalArgument {
    }

    @ServiceArgument
    public interface PostContraJournalArgument extends Argument {
        void setJournalArg(Journal journalArg);

        Journal getJournalArg();

        void setJournalTypeArg(JournalType journalType);

        JournalType getJournalTypeArg();

        void setReasonArg(String reasonArg);

        String getReasonArg();

        void setWrittenDateArg(Date writtenDate);

        Date getWrittenDateArg();

        void setContraRet(Journal contraRet);

        Journal getContraRet();
    }
}
