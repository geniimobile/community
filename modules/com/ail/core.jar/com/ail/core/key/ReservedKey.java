package com.ail.core.key;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.NamedQuery;

import com.ail.core.Type;

@Entity
@Table(uniqueConstraints=@UniqueConstraint(columnNames= {"rkeKeyId", "rkeValue"}))
@NamedQuery(name = "get.next.key.by.id", query = "select key from ReservedKey key where key.keyId = ?")
public class ReservedKey extends Type {
    @Column(name = "rkeKeyId")
    private String keyId;
    @Column(name = "rkeValue")
    private String value;

    ReservedKey() {
    }

    public ReservedKey(String keyId, String value) {
        this.keyId = keyId;
        this.value = value;
    }

    public String getKeyId() {
        return keyId;
    }

    public void setKeyId(String keyId) {
        this.keyId = keyId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
