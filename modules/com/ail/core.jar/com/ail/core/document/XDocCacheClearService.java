/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

package com.ail.core.document;

import com.ail.annotation.ServiceArgument;
import com.ail.annotation.ServiceCommand;
import com.ail.annotation.ServiceImplementation;
import com.ail.core.Functions;
import com.ail.core.Service;
import com.ail.core.command.Argument;
import com.ail.core.command.Command;
import com.ail.core.document.XDocCacheClearService.XDocCacheClearArgument;

import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;

/**
 * XDoc maintains a cache of reports (document templates) internally which is quite independent
 * of OU's product caching mechanism. This service provides a means for OU to selectively clear
 * the reports cache and therefore keep XDoc's cache in step with OU's.
 */
@ServiceImplementation
public class XDocCacheClearService extends Service<XDocCacheClearArgument> {

    @ServiceArgument
    public interface XDocCacheClearArgument extends Argument {
        String getProductNamespaceArg();

        void setProductNamespaceArg(String productNamespaceArg);
    }

    @ServiceCommand(defaultServiceClass = XDocCacheClearService.class)
    public interface XDocCacheClearCommand extends Command, XDocCacheClearArgument {
    }

    @Override
    public void invoke() {
        if (Functions.isEmpty(args.getProductNamespaceArg())) {
            XDocReportRegistry.getRegistry().clear();
        }
        else {
            String productPath = namespaceToPath();

            for(IXDocReport report: XDocReportRegistry.getRegistry().getCachedReports()) {
                if (report.getId().contains(productPath)) {
                    XDocReportRegistry.getRegistry().unregisterReport(report);
                }
            }
        }
    }

    /*
     * "Inshur.FHV.OwnerDriverUK.Registry" -> "/Inshur/FHV/OwnerDriverUK/"
     */
    String namespaceToPath() {
        return '/' + Functions.configurationNamespaceToProductName(args.getProductNamespaceArg()).replace('.', '/') + '/';
    }
}
