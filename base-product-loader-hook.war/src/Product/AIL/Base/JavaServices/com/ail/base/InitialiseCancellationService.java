/* Copyright Applied Industrial Logic Limited 2018. All rights reserved. */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.base;

import java.util.Calendar;
import java.util.Date;

import com.ail.core.product.ProductServiceCommand;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.quotation.InitialiseCancellationService.InitialiseCancellationArgument;
import com.ail.pageflow.PageFlowContext;

/**
 * Initialise a quotation in preparation for an Cancellation quotation. 
 */
@ProductServiceCommand(serviceName = "InitialiseCancellationService", commandName = "InitialiseCancellationCommand")
public class InitialiseCancellationService {

	public static void invoke(InitialiseCancellationArgument args) {
        Calendar date = null;
        Policy quotation = (Policy) args.getCancellationQuotationArg();

        /* Set the policy and expiry dates to today */
        quotation.setQuotationDate(new Date());
        quotation.setInceptionDate(new Date());

        /* Set the quotation expiry date to today + 30 days */
        date = Calendar.getInstance();
        date.add(Calendar.DATE, 30);
        quotation.setQuotationExpiryDate(date.getTime());

        /* Give the current user ownership - null is okay if the user is a guest */
        quotation.setOwningUser(PageFlowContext.getRemoteUser());
    }
}