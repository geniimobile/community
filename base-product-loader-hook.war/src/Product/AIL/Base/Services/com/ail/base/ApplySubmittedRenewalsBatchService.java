/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.base;

import static com.ail.core.CoreContext.getCoreProxy;
import static com.ail.core.CoreContext.getRemoteUser;
import static com.ail.core.CoreContext.getRequestWrapper;
import static com.ail.insurance.policy.PolicyLinkType.RENEWAL_QUOTATION_FROM;
import static com.ail.insurance.policy.PolicyStatus.SUBMITTED;
import static java.lang.String.format;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.util.Arrays.asList;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;

import com.ail.core.BaseException;
import com.ail.core.JSONException;
import com.ail.core.RestfulServiceInvoker;
import com.ail.core.RestfulServiceReturn;
import com.ail.core.jsonmapping.jackson.Mapper;
import com.ail.core.product.ProductServiceCommand;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.policy.PolicyLink;
import com.ail.insurance.search.PolicySearchService.PolicySearchCommand;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;

/**
 * A "batch" service which searches for and applies all renewal quotations which have a status of SUBMITTED
 * and an inception date on or before the data specified as an argument.
 * This services in turn uses the underlying ApplySubmittedRenewalService to apply each quotation that it matches.
 * As we are processing in bulk we need to take care to return details about all the attempts made to process
 * renewals whether they succeed or not. So we return two lists, one essentially passes on the results of the 
 * underlying service calls if they succeed, the other lists the errors if any are encountered.
 */
@ProductServiceCommand(serviceName = "ApplySubmittedRenewalsBatchService", commandName = "ApplySubmittedRenewalsBatch")
public class ApplySubmittedRenewalsBatchService extends RestfulServiceInvoker {

	// java.net.HttpURLConnection doesn't define multi-status - so we'll define our own.
	private static final int HTTP_MULTI_STATUS = 207;

	public static void invoke(ExecutePageActionArgument args) throws BaseException {
        new ApplySubmittedRenewalsBatchService().invoke(Argument.class);
    }

    public RestfulServiceReturn service(Argument arg) throws BaseException {
    	if (arg.inceptionDate == null) {
            return new Return(HTTP_BAD_REQUEST, "arg.inceptionDate == null");
    	}
    	
    	Return response = new Return(HTTP_OK);

		for (Policy renewingPolicy : fetchQuotationsSubmittedForRenewalOnOrBefore(arg.inceptionDate)) {
			try {
				URL applySubmittedRenewal = new URL(
												format("%s://%s:%s/policy/%s/ApplySubmittedRenewal",
													getRequestWrapper().getScheme(), 
													getRequestWrapper().getServerName(),
													getRequestWrapper().getServerPort(), 
													renewingPolicy.getExternalSystemId()
												)
											);
				
				JsonNode resultNode = parseJsonResult(openAuthenticatedConnection(applySubmittedRenewal));

				if (resultNode.get("success").asBoolean()) {
					response.renewals.add(new Return.Renewal(resultNode.get("renewal")));
				} else {
					response.errors.add(new Return.Error(renewingPolicy.getSystemId(), resultNode.get("message").asText()));
				}
			} catch (Throwable e) {
				response.errors.add(new Return.Error(renewingPolicy.getSystemId(), e.getMessage()));
			}
		}
    	
		if (response.errors.size() != 0) {
			response.returnStatus = (response.renewals.size() > 0) ? HTTP_MULTI_STATUS : HTTP_INTERNAL_ERROR;
		}
		
		return response;
    }
    
    // Open a connection to the URL specified reusing the authentication token that we were invoked with.
    private URLConnection openAuthenticatedConnection(URL url) throws IOException {
		URLConnection connection = url.openConnection();
		connection.setRequestProperty("Authorization", getRequestWrapper().getHeader("Authorization"));
		connection.connect();
		return connection;
    }
    
    private JsonNode parseJsonResult(URLConnection json) throws JSONException, JsonProcessingException, IOException {
    	return new Mapper().defaultMapper().readTree(json.getInputStream());
    }
    
	private Collection<Policy> fetchQuotationsSubmittedForRenewalOnOrBefore(Date inceptionDate) throws BaseException {
    	PolicySearchCommand psc = getCoreProxy().newCommand(PolicySearchCommand.class);
    	psc.setPolicyStatusArg(asList(SUBMITTED));
    	psc.setInceptionDateMaximumArg(inceptionDate);
    	psc.setUserIdArg(getRemoteUser());
    	psc.setIncludeTestArg(false);
    	psc.invoke();
    	
    	// From a list any policy that's submitted with the right inception date, we find those that are
    	// renewal quotations (pl.getLinkType() == RENEWAL_QUOTATION_FROM), and build a list of the IDs 
    	// of the policies that they relate to (i.e. the "master" policy IDs).
    	List<Policy> renewingPolicy = new ArrayList<>();
    	
    	for(Policy shortPolicy: psc.getPoliciesRet()) {
    		Policy fullPolicy = (Policy)getCoreProxy().queryUnique("get.policy.by.systemId", shortPolicy.getSystemId());
    		for(PolicyLink policyLink: fullPolicy.getPolicyLink()) {
    			if (policyLink.getLinkType() == RENEWAL_QUOTATION_FROM) {
    				Policy targetPolicy = (Policy)getCoreProxy().queryUnique("get.policy.by.systemId", policyLink.getTargetPolicyId());
    				renewingPolicy.add(targetPolicy);
    				break;
    			}
    		}
    	}

    	return renewingPolicy;
    }
    
    public static class Argument {
    	Date inceptionDate;
    }

    public static class Return extends RestfulServiceReturn {
        int count;
        String message = "";
        List<Renewal> renewals = new ArrayList<>();
        List<Error> errors = new ArrayList<>();

        public static class Renewal {
            Long policyUID;
            String inceptonDate;
            String expiryDate;
            String insuredName;
            String policyNumber;

            public Renewal(JsonNode renewalNode) {
            	this.policyUID = renewalNode.get("policyUID").asLong();
                this.inceptonDate = renewalNode.get("inceptonDate").asText();
                this.expiryDate = renewalNode.get("expiryDate").asText();
                this.insuredName = renewalNode.get("insuredName").asText();
                this.policyNumber = renewalNode.get("policyNumber").asText();;
            }
        }

        public static class Error {
            Long policyUID;
            String details;

            public Error(Long policyUID, String details) {
            	this.policyUID = policyUID;
            	this.details = details;
            }
        }

        public Return(int status) {
        	super(status);
        }

        public Return(int status, String message) {
            super(status);
            this.message = message;
        }
    }
}