/* Copyright Applied Industrial Logic Limited 2007. All rights reserved. */
import com.ail.insurance.policy.Policy;
import com.ail.insurance.policy.PolicyStatus;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.pageflow.PageFlowContext;

public class DocumentButtonActionPolicyUpdateService {

	public static void invoke(ExecutePageActionArgument args) {
		new DocumentButtonActionPolicyUpdateService().service(args);
    }
	
	private void service(ExecutePageActionArgument args) {
    	Policy policy = PageFlowContext.getPolicy();

    	handleQuoteNumberOverride(policy);
        
    	handlePolicyStatusOverride(policy);

    	handlePolicyNumberOverride(policy);
    	
    	clearFields(policy);
	}

	private void handleQuoteNumberOverride(Policy policy) {
    	String quoteNumberOverride = (String) policy.xpathGet("/asset[id='commandActionButton']/attribute[id='quoteNumberOverride']/value");
    	if (!com.ail.core.Functions.isEmpty(quoteNumberOverride)) {
    		policy.setQuotationNumber(quoteNumberOverride);
		}
    }

    private void handlePolicyNumberOverride(Policy policy) {
    	String policyNumberOverride = (String) policy.xpathGet("/asset[id='commandActionButton']/attribute[id='policyNumberOverride']/value");
    	if (!com.ail.core.Functions.isEmpty(policyNumberOverride)) {
           	policy.setPolicyNumber(policyNumberOverride);
		}
    }

    private void handlePolicyStatusOverride(Policy policy) {
    	String policyStatusOverride = (String) policy.xpathGet("/asset[id='commandActionButton']/attribute[id='policyStatusOverride']/formattedValue");

        if (!"?".equals(policyStatusOverride)) {
        	policy.setStatus(PolicyStatus.forName(policyStatusOverride));
        }
    }

    private void clearFields(Policy policy) {
    	policy.xpathSet("/asset[id='commandActionButton']/attribute[id='quoteNumberOverride']/value", "");
    	policy.xpathSet("/asset[id='commandActionButton']/attribute[id='policyNumberOverride']/value", "");
    	policy.xpathSet("/asset[id='commandActionButton']/attribute[id='policyStatusOverride']/value", "?");
	}
}